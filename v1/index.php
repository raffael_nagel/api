<?php

require_once "../lib/vendor/autoload.php";
require "config.php";

spl_autoload_register(function ($classname) {
    require ("/src/" . $classname . ".php");
});




/**
 *
 *      basePath="/v1",
 * 		host="http://www.raffael.nagel.link/api",
 * 		schemes={"http"},
 * 		produces={"application/json"},
 * 		consumes={"application/json"},
 * 		@SWG\Info(
 * 			title="My API",
 * 			description="REST API",
 * 			version="0.1",
 * 			termsOfService="terms",
 * 		),
 */

use \Slim\App;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new App();
$container = $app->getContainer();

/*** Setup Logger  ***/
$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};


$app->add(function (Request $request, Response $response, $next) {

//    $this->logger->addInfo("Something interesting happened");

    //Retrieve the public key from the request
    if (!$request->hasHeader('X-API-KEY') || !$request->hasHeader('X-API-TIMESTAMP') || !$request->hasHeader('X-API-REQUEST')) {
        return $this->response->withStatus(400,HTTP_400_STATUS)->withJson(
            array(
                "error" => 400,
                "error_message" => HTTP_400_MSG
            )
        );
    }

    $key = $request->getHeaderLine('X-API-KEY');

    //check timestamp YYYYMMDD'T'HHMMSS'Z'
    $clientTimestamp = $request->getHeaderLine('X-API-TIMESTAMP');

    if(!Authentication::checkTimestampDiff($clientTimestamp)){
        return $this->response->withStatus(410, HTTP_410_STATUS)->withJson(
            array(
                "error" => 403,
                "error_message" => HTTP_410_MSG
            )
        );
    }

    //retrieve private key from database
    $privateKey = Authentication::getPrivateKey($key);
    if(!$privateKey){
        return $this->response->withStatus(403, HTTP_403_STATUS)->withJson(
            array(
                "error" => 403,
                "error_message" => HTTP_403_MSG,
            )
        );
    }
    //encrypt and compare
    $encriptedRequest = Authentication::encryptRequest($request->getUri()->getPath(),$clientTimestamp,$privateKey);
    if($encriptedRequest != $request->getHeaderLine('X-API-REQUEST')){
        return $this->response->withStatus(403, HTTP_403_STATUS)->withJson(
            array(
                "error" => 403,
                "error_message" => HTTP_403_MSG
            )
        );
    }

    $response->withStatus(200, 'OK');
    $request = $request->withAttribute('key',$key);
    $response = $next($request, $response);

    return $response;
});

/**
 * @SWG\Get(
 *     path="/",
 *     @SWG\Response(response="200", description="Welcome: This is My Api")
 * )
 */
$app->get('/', function (Request $request, Response $response, $args) {
    return $this->response->withStatus(200,HTTP_200_STATUS)->withJson(array(
        "message" => "Welcome: This is My Api v0.1"
    ));
});

/**
 * @SWG\Get(
 *     path="/balance",
 *     @SWG\Response(response="200", description="Content of wallet")
 * )
 */
$app->get('/balance', function (Request $request, Response $response, $args) {
    $apiKey = $request->getAttribute('key');
    $moneyController = new MoneyController();

    $balance = $moneyController->getBalance($apiKey);

    if($balance === false){
        return $this->response->withStatus(501, HTTP_501_STATUS)->withJson(
            array(
                "error" => 501,
                "error_message" => HTTP_501_MSG
            )
        );
    }

    return $this->response->withStatus(200,HTTP_200_STATUS)->withJson(array("Balance" => $balance));
});

/**
 * @SWG\Get(
 *     path="/transactions",
 *     @SWG\Response(response="200", description="List of transactions")
 * )
 */
$app->get('/transactions', function (Request $request, Response $response, $args) {

    $param = $request->getQueryParams();

    $filter = isset($param['filter']) ? json_decode($param['filter']) : "";
    $order  = isset($param['order']) ? $param['order'] : "";

    $apiKey = $request->getAttribute('key');
    $moneyController = new MoneyController($apiKey);
    $transactions = $moneyController->getTransactions($apiKey,$filter,$order);

    if($transactions === false){
        return $this->response->withStatus(501, HTTP_501_STATUS)->withJson(
            array(
                "error" => 501,
                "error_message" => HTTP_501_MSG
            )
        );
    }

    return $this->response->withStatus(200,HTTP_200_STATUS)->withJson(array(
        "Transactions" => $transactions
    ));
});

/**
 * @SWG\Post(
 *     path="/transaction",
 *     @SWG\Response(response="200", description="OK")
 * )
 */
$app->post('/transaction', function (Request $request, Response $response, $args) {
    $data = $request->getQueryParams();
    $apiKey = $request->getAttribute('key');

    if(!array_key_exists('operation',$data) || !array_key_exists('amount',$data)){
        return $this->response->withStatus(400, HTTP_400_STATUS)->withJson(
            array(
                "error" => 400,
                "error_message" => HTTP_400_MSG
            )
        );
    }

    $ticket_data = [];
    $ticket_data['operation'] = strtoupper(filter_var($data['operation'], FILTER_SANITIZE_STRING));
    $ticket_data['amount'] = filter_var($data['amount'], FILTER_VALIDATE_FLOAT);

    if(!in_array($ticket_data['operation'], array('DEPOSIT','WITHDRAW'))){
        return $this->response->withStatus(400, HTTP_400_STATUS)->withJson(
            array(
                "error" => 400,
                "error_message" => HTTP_400_MSG
            )
        );
    }

    $moneyController = new MoneyController();
    if(!$moneyController->insertTransaction($apiKey,$ticket_data)){
        return $this->response->withStatus(501, HTTP_501_STATUS)->withJson(
            array(
                "error" => 501,
                "error_message" => HTTP_501_MSG
            )
        );
    }

    $previousBalance = $moneyController->getBalance($apiKey)['balance'];
    $newBalance = 0;
    switch ($ticket_data['operation']){
        case "DEPOSIT":
            $newBalance = $previousBalance + floatval($ticket_data['amount']);
            break;
        case "WITHDRAW":
            $newBalance = $previousBalance - $ticket_data['amount'];
            break;
    }
    if(!$moneyController->newBalance($apiKey,$newBalance)){
        return $this->response->withStatus(501, HTTP_501_STATUS)->withJson(
            array(
                "error" => 501,
                "error_message" => HTTP_501_MSG
            )
        );
    }

    return $this->response->withStatus(200,HTTP_200_STATUS)->withJson(array(
        "success" => "Hey, you just ".$data['operation']." $".$ticket_data['amount'],
        "operation" => $newBalance,
        "amount" => $newBalance,
        "balance" => $newBalance));
});


//Other functions-------------------------------------------------------------------------------------------------------

/**
 * @SWG\Post(
 *     path="/api/sendmail",
 *     @SWG\Response(response="200", description="Content of My Wallet")
 * )
 */
$app->post('/sendmail', function (Request $request, Response $response) {
    $data = $request->getParsedBody();

    $ticket_data['message'] = filter_var($data['message'], FILTER_SANITIZE_STRING);

    $mail = new MailController();
    if($ret = $mail->sendEmail($ticket_data['message'])){
        $this->response = $response->withStatus(200,'Success');
        return $this->response->withJson(array("success" => "email sent."));
    }else{
        return $response->withStatus(404,'Error');
    }


});


$app->run();
?>