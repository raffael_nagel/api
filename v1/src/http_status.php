<?php


define('HTTP_200_STATUS','Success');


define('HTTP_400_STATUS','Bad Request');
define('HTTP_400_MSG','Hey Man, you are not doing this right.');

define('HTTP_403_STATUS','Unauthorized Call');
define('HTTP_403_MSG','Hey Man, you can not do this.');

define('HTTP_410_STATUS','Gone');
define('HTTP_410_MSG','Hey Man, your time is over.');

define('HTTP_501_STATUS','Server Error');
define('HTTP_501_MSG','Sorry Pal, we got an error.');

?>