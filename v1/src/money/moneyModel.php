<?php

require_once ROOT.'/src/database/db.php';
require_once ROOT.'/src/stringTools.php';

class MoneyModel{

    private $db;

    function __construct(){
        $this->db = new Database();
        $this->db->connect();
    }

    function getTransactions($token, $filter, $order){

        $database = $this->db->getDatabase();

        $query = "select ID,DATE,OPERATION,AMOUNT,CURRENCY from TRANSACTIONS 
                                         join API_ACCESS on TRANSACTIONS.USER_ID = API_ACCESS.USER_ID 
                                         where API_ACCESS.TOKEN = :token ";

        if($filter != "" ){
            foreach($filter as $column => $value){
                $query .= " AND " . $column ." = :". $column;
            }
        }

        if($order != ""){
            $order = strtoupper($order);
            $orders = array("DATE","AMOUNT","OPERATION");
            $key = array_search($order,$orders);
            $order = $orders[$key];
            $query .= " ORDER BY $order" ;
        }

        $statement = $database->prepare($query);

        $statement->bindParam(':token', $token);

        if($filter != ""){
            foreach($filter as $column => $value){
                $statement->bindParam(':'.$column, $value);
            }
        }

        $statement->execute();

        if ($statement->rowCount() > 0){

            $return = array();

            while($data = $statement->fetch( PDO::FETCH_ASSOC )){
                $transaction['date'] = $data['DATE'];
                $transaction['operation'] = $data['OPERATION'];
                $transaction['amount'] = '$'.StringTools::FormatCurrency($data['AMOUNT']);
                $transaction['currency'] = $data['CURRENCY'];

                $return['T'.$data['ID']] = $transaction;
            }
            return $return;

        }else{
            return false;
        }
    }

    function getBalance($token){

        if($token == '')return false;

        $database = $this->db->getDatabase();

        $statement = $database->prepare("select DATE as date,BALANCE as balance from WALLET_BALANCE JOIN 
                                         API_ACCESS ON API_ACCESS.USER_ID = WALLET_BALANCE.USER_ID
                                         where TOKEN = :token ORDER BY DATE DESC LIMIT 1");
        $statement->bindParam(':token', $token);
        $statement->execute();

        if ($statement->rowCount() > 0){
            $data = $statement->fetch(PDO::FETCH_ASSOC);
//            $data['balance'] = '$'.StringTools::FormatCurrency($data['balance']);
            return $data;
        }else{
            return false;
        }
    }

    function newTransaction($token,$data){

        if($token == '')return false;

        if(!isset($data['operation']) || !isset($data['amount'])) return false;

        if(!isset($data['currency']))
            $data['currency'] = "USD";


        $database = $this->db->getDatabase();
        $statement = $database->prepare("INSERT INTO TRANSACTIONS(USER_ID,DATE,OPERATION,AMOUNT,CURRENCY) 
                                         VALUES(
                                         (SELECT USER_ID FROM API_ACCESS WHERE TOKEN = :token),
                                         NOW(),:operation,:amount,:currency) ");
        $statement->bindParam(':token', $token);
        $statement->bindParam(':operation', $data['operation']);
        $statement->bindValue(':amount', floatval($data['amount']),PDO::PARAM_INT);
        $statement->bindParam(':currency', $data['currency']);

        return $statement->execute();
    }

    function newBalance($token,$amount){

        if($token == '')return false;

        $database = $this->db->getDatabase();
        $statement = $database->prepare("INSERT INTO WALLET_BALANCE(USER_ID,DATE,BALANCE) 
                                         VALUES(
                                         (SELECT USER_ID FROM API_ACCESS WHERE TOKEN = :token),
                                         NOW(),:amount) ");
        $statement->bindParam(':token', $token);
        $statement->bindValue(':amount', floatval($amount),PDO::PARAM_INT);

        return $statement->execute();
    }


}




?>