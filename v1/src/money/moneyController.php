<?php

require_once 'moneyModel.php';

class MoneyController{

    private $Model;

    function __construct(){
        $this->Model = new MoneyModel();
    }

    function getTransactions($token, $filter = "", $order = ""){
        return $this->Model->getTransactions($token,$filter,$order);
    }

    function getBalance($token){
        return $this->Model->getBalance($token);
    }

    function newBalance($token,$amount){
        return $this->Model->newBalance($token,$amount);
    }

    function insertTransaction($token,$data){
        return $this->Model->newTransaction($token,$data);
    }

}


?>