<?php

require_once "authenticationModel.php";

/**
 * Created by PhpStorm.
 * User: Raffael
 * Date: 23/10/2016
 * Time: 20:18
 */
class Authentication
{

    public static function getPrivateKey($token){

        $model = new AuthenticationModel();

        $secret = $model->getSecret($token);
        if($secret !== false){
            return $secret;
        }else{
            return false;
        }
    }

    public static function checkTimestampDiff($timestamp){
        if(is_array($timestamp))$timestamp = implode('',$timestamp);
        $timestamp = strtotime($timestamp);
        date_default_timezone_set('America/Los_Angeles');
        $date = new DateTime();
        $date->setTimestamp($timestamp);
        $difference = $date->diff(new DateTime());

        $minutes = $difference->days * 24 * 60 + $difference->i;

        return $minutes < 12 && $minutes > -1;
    }

    public static function encryptRequest($request,$timestamp,$privateKey){
        if($request == null || $timestamp == null) return false;
        $timestamp = StringTools::clean($timestamp);
        $signature = base64_encode(hash_hmac("sha256", $request.'_'.$timestamp, $privateKey, True));
        return $signature;
    }


    public static function generateSecret($token){
        $timestamp = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());

        return base64_encode(hash_hmac("sha512", $timestamp.openssl_random_pseudo_bytes(50), $token, True));
    }

    public static function generateToken(){
        $timestamp = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
        return StringTools::clean(base64_encode(openssl_random_pseudo_bytes(10).$timestamp));
    }


}