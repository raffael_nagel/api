<?php

require_once ROOT.'/src/database/db.php';

/**
 * Created by PhpStorm.
 * User: Raffael
 * Date: 24/10/2016
 * Time: 19:22
 */
class AuthenticationModel
{

    private $db;

    function __construct(){
        $this->db = new Database();
        $this->db->connect();
    }

    function getSecret($token){
        $database = $this->db->getDatabase();

        $statement = $database->prepare("select SECRET from API_ACCESS where TOKEN = :token LIMIT 1");
        $statement->bindParam(':token', $token);
        $statement->execute();

        if ($statement->rowCount() > 0){
            $data = $statement->fetch(PDO::FETCH_ASSOC);
            return $data['SECRET'];
        }else{
            return false;
        }
    }


}