<?php

require_once ROOT.'/vendor/php-mailer/PHPMailerAutoload.php';

/**
 * Created by PhpStorm.
 * User: Raffael
 * Date: 20/10/2016
 * Time: 18:34
 */
class MailModel{

    private $mail;

    public function __construct($host='email-ssl.com.br',
                                $username="raffael@lapwing.com.br",
                                $password="72hhj8yr4b",
                                $smtpType="ssl",$port="465"){

        $mail = new PHPMailer();

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->isSMTP();
        $mail->Host = $host;
        $mail->SMTPAuth = true;
        $mail->Username = $username;
        $mail->Password = $password;
        $mail->SMTPSecure = $smtpType;
        $mail->Port = $port;

        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';

        $this->mail = $mail;

    }

    public function sendMail($message,$subject,$to,$from,$fromName){

        $this->mail->addAddress($to);
        $this->mail->From = $from;
        $this->mail->FromName = $fromName;

        $this->mail->Subject = $subject;

        if(get_magic_quotes_gpc()) {
            $message = stripslashes($message);
        }

        $this->mail->Body = $message;

        if(!$this->mail->Send()) {
            return($this->mail->ErrorInfo);
        }else {
            return(true);
        }
    }

}