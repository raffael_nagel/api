<?php

require_once "mailModel.php";

/**
 * Created by PhpStorm.
 * User: Raffael
 * Date: 20/10/2016
 * Time: 18:30
 */
class MailController{

    private $Model;

    function __construct(){
        $this->Model = new MailModel();
    }

    function sendEmail($message="Hello Master",$subject="API",$to = "raffael.nagel@gmail.com",$from = "raffael@lapwing.com.br",$fromName="Raffael Nagel"){
        return $this->Model->sendMail($message,$subject,$to,$from,$fromName);
    }

}