<?php

class StringTools
{

    public static function encript($word, $salt)
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encoded = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $salt, $word, MCRYPT_MODE_CBC, $iv);
        $encoded = $iv . $encoded;

        return urlencode($encoded);
    }

    public static function decript($word, $salt)
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $decript = urldecode($word);
        $iv_dec = substr($decript, 0, $iv_size);
        $decript = substr($decript, $iv_size);

        return mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $salt, $decript, MCRYPT_MODE_CBC, $iv_dec);
    }

    public static function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '', $string); // Replaces multiple hyphens with single one.
    }

    /**
     * Retorna somente caracteres numéricos de uma string
     * @param string $texto
     * @return type
     */
    public static function OnlyNumbers($text) {
        return  preg_replace("/[^0-9]/","", $text);
    }


    /**
     * Converte para o formato em Reais para visualização em Layouts
     * @param string $numero
     * @return string
     */
    public static function FormatCurrency($numero) {

        if (strpos($numero, '.') === false && strpos($numero, ',') === false) {
            $numero .= '00';
        }
        foreach(['.',","] as $v){
            if (strpos($numero, $v) !== false) {
                $decPoint = explode($v, $numero);
                if (count($decPoint) > 1) {
                    $ponto = end($decPoint);
                    if (strlen($ponto) == 1) {
                        $numero .= '0';
                    }
                }
            }
        }

        $numero = self::OnlyNumbers($numero);

        $result = '';
        $i = strlen($numero) - 1;
        $p = 0;
        $c = 0;
        while ($i >= 0) {
            $p++;
            $c++;
            if ($p == 3) {
                $result = ',' . $result;
                $c = 0;
            }
            if ($c == 3) {
                $result = '.' . $result;
                $c = 0;
            }
            $result = $numero[$i] . $result;
            $i--;
        }
        $result = str_replace(',',';',$result);
        $result = str_replace('.',',',$result);
        $result = str_replace(';','.',$result);
        return $result;
    }

}
?>