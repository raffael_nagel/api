<?php

require_once 'walletModel.php';
class WalletController{

    private $Model;
    private $token;

    function __construct($token){
        $this->Model = new WalletModel();
        $this->token = $token;
    }

    function getWallet(){
        return $this->Model->getWalletInfo($this->token);
    }

    function getWalletId(){
        return $this->Model->getId($this->token);
    }
}


?>