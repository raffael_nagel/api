<?php

define('DB_NAME', 'MasterDB');
define('DB_USER', 'raffael_nagel');
define('DB_PASSWORD', '72hhj8yr4b');
define('DB_HOST', 'masterdatabase.cfo6iccaoz9o.us-east-1.rds.amazonaws.com');


class Database{

    private $DSN;
    private $OPTIONS;
    private $DB;

    function __contruct(){
        $this->OPTIONS = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
    }

    function connect() {
        try {
            $this->DSN = "mysql:host=".DB_HOST.";dbname=".DB_NAME;
            $this->DB = new PDO($this->DSN, DB_USER, DB_PASSWORD, $this->OPTIONS);
        }catch (PDOexception $e){
            echo "Sorry Master, but I can't do this right now.";
            throw $e;
        }
    }

    function getDatabase(){
        return $this->DB;
    }

    function select($table,$cols,$where = '',$order = '',$pag = 0,$total = 50,$debug = 0){

        if(is_array($cols)) {
            $select = 'SELECT ' . implode(',', $cols) . ' FROM  ' . $table . ' ';
        }else{
            $select = 'SELECT ' . $cols . ' FROM  ' . $table . ' ';
        }

        if($where != ''){
            $select .= ' WHERE '.$where;
        }

        if($order != ''){
            $select .= ' ORDER BY '.$order;
        }

        if($total > 0) {
            $select .= ' LIMIT ' . $pag . ',' . $total;
        }

        if($debug == 1) return $select;

        $query = $this->DB->query($select);

        $result = array();
        $i = 0;
        try {
            while ($dados = $query->fetch()) {
                if (is_array($cols)) {
                    foreach ($cols as $column) {
                        if (strpos($column, 'as ') !== false) {
                            $column = substr($column, strpos($column, 'as ') + 3, strlen($column));
                        }
                        $result[$i][$column] = utf8_encode($dados[$column]);
                    }
                } else {
                    if (strpos($cols, 'as ') !== false) {
                        $cols = substr($cols, strpos($cols, 'as ') + 3, strlen($cols));
                    }
                    $result[$i][$cols] = utf8_encode($dados[$cols]);
                }
                $i++;
            }
        }catch (PDOException $e){
            throw $e;
        }
        return $result;
    }

    function update($table,$where,$data){

        $values = array();
        foreach($data as $key => $value){
            $values[] = $key .' = '. $value ;
        }
        $set = implode(', ', $values);
        $sql = 'UPDATE '.$table.' SET '.$set. ' WHERE '.$where;

        try{
            $this->DB->exec($sql);
        }catch (PDOexception $e){
            echo "Sorry Master, but I can't do this right now.";
            throw $e;
        }
    }

    function insert($table,$data){
        $sql = 'INSERT INTO '.$table. ' ('.implode(', ',array_keys($data)).') VALUES('.implode(', ',array_values($data)).')';

        try{
            $this->DB->exec($sql);
        }catch (PDOexception $e){
            echo "Sorry Master, but I can't do this right now.";
            throw $e;
        }
    }

    function delete($table,$where){
        $sql = 'DELETE FROM '.$table.' WHERE '.$where;

        try{
            $this->DB->exec($sql);
        }catch (PDOexception $e){
            echo "Sorry Master, but I can't do this right now.";
            throw $e;
        }
    }

    function close(){
        $this->DB = null;
    }

}


?>