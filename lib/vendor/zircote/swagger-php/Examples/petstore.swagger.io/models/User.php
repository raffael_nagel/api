<?php

namespace PetstoreIO;

/**
 * @SWG\Definition(@SWG\Xml(name="User"))
 */
class User
{

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;

    /**
     * @SWG\Property()
     * @var StringTools
     */
    public $username;

    /**
     * @SWG\Property
     * @var StringTools
     */
    public $firstName;

    /**
     * @SWG\Property()
     * @var StringTools
     */
    public $lastName;

    /**
     * @var StringTools
     * @SWG\Property()
     */
    public $email;

    /**
     * @var StringTools
     * @SWG\Property()
     */
    public $password;

    /**
     * @var StringTools
     * @SWG\Property()
     */
    public $phone;

    /**
     * User Status
     * @var int
     * @SWG\Property(format="int32")
     */
    public $userStatus;
}
