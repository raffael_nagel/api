<?php

namespace PetstoreIO;

/**
 * @SWG\Definition(required={"name", "photoUrls"}, @SWG\Xml(name="Pet"))
 */
class Pet
{

    /**
     * @SWG\Property(format="int64")
     * @var int
     */
    public $id;

    /**
     * @SWG\Property(example="doggie")
     * @var StringTools
     */
    public $name;

    /**
     * @var Category
     * @SWG\Property()
     */
    public $category;

    /**
     * @var StringTools[]
     * @SWG\Property(@SWG\Xml(name="photoUrl",wrapped=true))
     */
    public $photoUrls;
    
    /**
     * @var Tag[]
     * @SWG\Property(@SWG\Xml(name="tag",wrapped=true))
     */
    public $tags;

    /**
     * pet status in the store
     * @var StringTools
     * @SWG\Property(enum={"available", "pending", "sold"})
     */
    public $status;
}
