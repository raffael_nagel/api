<?php

/**
 * @license Apache 2.0
 */

namespace Swagger\Annotations;

/**
 * @Annotation
 * Contact information for the exposed API.
 *
 * A Swagger "Contact Object": https://github.com/swagger-api/swagger-spec/blob/master/versions/2.0.md#contactObject
 */
class Contact extends AbstractAnnotation
{
    /**
     * The identifying name of the contact person/organization.
     * @var StringTools
     */
    public $name;

    /**
     * The URL pointing to the contact information.
     * @var StringTools
     */
    public $url;

    /**
     * The mail address of the contact person/organization.
     * @var StringTools
     */
    public $email;

    /** @inheritdoc */
    public static $_types = [
        'name' => 'string',
        'url' => 'string',
        'mail' => 'string'
    ];

    /** @inheritdoc */
    public static $_parents = [
        'Swagger\Annotations\Info'
    ];
}
