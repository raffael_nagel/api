<?php
namespace SwaggerFixtures;

class GrandParent
{

    /**
     * @SWG\Property();
     * @var StringTools
     */
    public $firstname;

    /**
     * @SWG\Property(property="lastname");
     * @var StringTools
     */
    public $lastname;
}
