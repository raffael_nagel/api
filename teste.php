<?php

require 'v1/config.php';

function ApiRequest($requestType = "GET",$requestMethod,$requestURL,$privateKey){

    $timestamp = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
    $signature = base64_encode(hash_hmac("sha256", $requestMethod.'_'.StringTools::clean($timestamp), $privateKey, True));

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://www.raffael.nagel.link/api/v1/".$requestURL,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $requestType,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "x-api-key: VwRpAt8euUFc1zIwMTYtMTAtMjVUMDI6MTg6MzQuMDAwWg",
            "x-api-request: $signature",
            "x-api-timestamp: $timestamp"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return "cURL Error #:" . $err;
    } else {
        return $response;
    }
}


$privateKey = "s82MFcdB5PhkwKvrRjOzSfzfpe+9jDLN+x+f9j/fGujbABAbM8v3rSrUHMs/UlasUhCjVPFXRD+PMxU5LD9RZQ==";

echo '<br/><br/><br/>';

//print_r(ApiRequest('POST','transactions','transactions?operation=deposit&amount=200',$privateKey));

print_r(ApiRequest('GET','transactions','transactions?filter={"operation":"WITHDRAW"}&order=DATE',$privateKey));

?>